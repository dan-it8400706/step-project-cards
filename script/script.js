// const a = {
//     id: 1,
//     title: 'Визит к кардиологу',
//     description: 'Новое описание визита',
//     doctor: 'Cardiologist',
//     bp: '24',
//     age: 23,
//     weight: 70
//   }

//  'Визит к кардиологу','Плановый визит', High, Ivanov, Ivan, Archipovich , Cardiologist, "120/60", 12, "no", 70
 

    // class Modal{
    //     constructor()
    // }

    class Visit{
                // мета візиту
                // короткий опис візиту
                // дропдаун - терміновість (звичайна, пріоритетна, невідкладна)
                // ПІБ
        constructor(targetOfVisit, whatHappened, urgency, firstName, lastName, surName){
            this.targetOfVisit    = targetOfVisit,
            this.whatHappened     = whatHappened,
            this.urgency          = urgency,
            this.firstName        = firstName,
            this.lastName         = lastName,
            this.surName          = surName
        }
        render(){
            // let card = `       
            // <div class="card" id="${this.id}">
            // <h4 class="title">${this.title}</h4>               
            // <p class="user">                      
            //     <span class="name">${this.name}</span>        
            //     <span class="username">${this.username}</span>    
            // </p>
            // <p class="text">${this.text}</p>                  
            // <p class="email">${this.email}</p> 
            // <buton class="btn" id="${this.id}">delete</buton>
            // </div>`
            // return card

//             ПІБ, які були введені під час створення картки
// Лікар, до якого людина записана на прийом
// Кнопка Показати більше. 
// На кліку на неї картка розширюється,
//  і з'являється решта інформації, яка була введена під час створення візиту

// Кнопка Редагувати. 
// При натисканні на неї замість текстового вмісту картки з'являється форма, 
// де можна відредагувати введені поля. Така ж, як у модальному вікні під час створення картки
// Іконка з хрестиком у верхньому правому кутку, при натисканні на яку картку буде видалено
        }
    }

    class VisitCardiologist extends Visit {
        constructor(targetOfVisit, whatHappened, urgency, firstName, lastName, surName, specialist, pressure, indexOfWeight, diseasesOfCardiovascularSystem, age, id ){
         super(targetOfVisit, whatHappened, urgency, firstName, lastName, surName),
            // super(whatHappened),
            // super(urgency),
            // super(firstName),
            // super(lastName),
            // super(surName),
            this.specialist = specialist,
            this.pressure   = pressure,
            this.indexOfWeight  = indexOfWeight,
            this. diseasesOfCardiovascularSystem = diseasesOfCardiovascularSystem,
            this.age = age,
            this.id = id   
        }
        render(){
            let card = `       
            <div class="card" id="${this.id}">
            <h4 class="title">${this.firstName} ${this.lastName} ${this.surName}</h4>               
            <p class="specialist">${this.specialist}</p>
            <button class="more" id="more">More</button>
            <p class ="targetOfVisit">${this.targetOfVisit}</p>
            <p class="whatHappened">${this.whatHappened}</p>
            <p class="urgency">${this.urgency}</p>
            <p class="pressure">${this.pressure}</p>
            <p class="indexOfWeight">${this.indexOfWeight}</p>
            <p class="diseasesOfCardiovascularSystem">${this.diseasesOfCardiovascularSystem}</p>
            <p class="age">${this.age}</p>
            <button class="rewrite">Rewrite</button>
            </div>`
            return card
        }
    }          
                //Кардіолог Class Visit +
                // звичайний тиск
                // Індекс маси тіла
                // перенесені захворювання серцево-судинної системи
                // вік

    
    
    class VisitDentist extends Visit {
        constructor(targetOfVisit, whatHappened, urgency, firstName, lastName, surName, specialist, dateOfLastVisit, id){
            super(targetOfVisit, whatHappened, urgency, firstName, lastName, surName),
            // super(whatHappened),
            // super(urgency),
            // super(firstName),
            // super(lastName),
            // super(surName),
            this.specialist = specialist,
            this.dateOfLastVisit = dateOfLastVisit,
            this.id = id
        }
        render(){
            let card = `       
            <div class="card" id="${this.id}">
            <h4 class="title">${this.firstName} ${this.lastName} ${this.surName}</h4>               
            <p class="specialist">${this.specialist}</p>
            <button class="more" id="more">More</button>
            <p class ="targetOfVisit">${this.targetOfVisit}</p>
            <p class="whatHappened">${this.whatHappened}</p>
            <p class="urgency">${this.urgency}</p>
            <p class="dateOfLastVisit">${this.dateOfLastVisit}</p>
            <button class="rewrite">Rewrite</button>
            </div>`
            return card
        }     
        
    }
                //Стоматолог, 
                // дата останнього відвідування



    class VisitTherapist extends Visit {
        constructor(targetOfVisit, whatHappened, urgency, firstName, lastName, surName, specialist, age, id){
            super(targetOfVisit, whatHappened, urgency, firstName, lastName, surName),
            // super(whatHappened),
            // super(urgency),
            // super(firstName),
            // super(lastName),
            // super(surName),
            this.specialist = specialist,
            this.age = age,
            this.id = id
        }
        render(){
            let card = `       
            <div class="card" id="${this.id}">
            <h4 class="title">${this.firstName} ${this.lastName} ${this.surName}</h4>               
            <p class="specialist">${this.specialist}</p>
            <button class="more" id="more">More</button>
            <p class ="targetOfVisit">${this.targetOfVisit}</p>
            <p class="whatHappened">${this.whatHappened}</p>
            <p class="age">${this.age}</p>
            <button class="rewrite">Rewrite</button>
            </div>`
            return card
        }
    }            
                // Терапевт
                // вік


let ivanToCardiologist = new VisitCardiologist('Визит до кардиологу','Плановий візит', 'High', 'Ivanov', 'Ivan', 'Archipovich' , 'Cardiologist', "120/60", 12, "no", 70, 1)
console.log(ivanToCardiologist)
document.querySelector('#test1').innerHTML = ivanToCardiologist.render()

let andreyToDantist = new VisitDentist('Визит до дантиста','Плановий візит', 'High', 'Pavlenko', 'Andrey', 'Yrievich' , 'Dantist', '21.12.2019', 2)
console.log(andreyToDantist)
document.querySelector('#test2').innerHTML = andreyToDantist.render()

let rimmaToTherapist = new VisitTherapist('Визит до теропевта','Плановий візит', 'High', 'Kyzmenko', 'Rimma', 'Mykolaivna' , 'Therapist', 19)
console.log(rimmaToTherapist)
document.querySelector('#test3').innerHTML = rimmaToTherapist.render()




// По кліку на кнопку Створити візит з'являється модальне вікно,
//  в якому можна створити нову картку.

// Модальне вікно "Створити візит"
// У модальному вікні повинні бути:

    // Випадаючий список (select) з вибором лікаря.   
       // У списку має бути три опції - Кардіолог, Стоматолог, Терапевт.
    //  Залежно від обраного лікаря, під цим списком будуть з'являтися поля, які потрібно дозаповнити для візиту до цього лікаря.

    // Після вибору лікаря зі списку, під ним повинні з'явитися поля для запису до цього лікаря. 
    //  Декілька полів є однаковими для всіх трьох докторів:
        // мета візиту
        // короткий опис візиту
        // дропдаун - терміновість (звичайна, пріоритетна, невідкладна)
        // ПІБ
        
    // Також кожен з лікарів має свої унікальні поля для заповнення. 
        // Якщо вибрано опцію Кардіолог, додатково з'являються такі поля для введення інформації:
            // звичайний тиск
            // Індекс маси тіла
            // перенесені захворювання серцево-судинної системи
            // вік
        
        // Якщо вибрано опцію Стоматолог, додатково необхідно заповнити:
            // дата останнього відвідування
        
        // Якщо вибрано опцію Терапевт, додатково необхідно заповнити:
            // вік

    // Кнопка Створити. 
    // При натисканні на кнопку надсилається AJAX запит на відповідну адресу, 
    // і якщо у відповіді надійшла інформація про новостворену картку -
    //  створюється картка у Дошці візитів на сторінці,
    //  модальне вікно закривається.

        // Кнопка Закрити - закриває модальне вікно без збереження інформації та створення картки.
    //  По кліку на область поза модальним вікном - модальне вікно також закривається.

    // Усі поля вводу, незалежно від вибраної опції, крім поля для додаткових коментарів, є обов'язковими для введення даних. Валідацію на коректність даних робити необов'язково.





    //     Картка, що описує візит У ній мають бути:

// ПІБ, які були введені під час створення картки
// Лікар, до якого людина записана на прийом

// Кнопка Показати більше. На кліку на неї картка розширюється, і з'являється решта інформації,
//  яка була введена під час створення візиту

// Кнопка Редагувати. При натисканні на неї замість текстового вмісту картки з'являється форма,
//  де можна відредагувати введені поля.
//  Така ж, як у модальному вікні під час створення картки

// Іконка з хрестиком у верхньому правому кутку, при натисканні на яку картку буде видалено
    




//     // Создание карточки


// Для создания карточки после заполнения формы в модальном окне вам 
// нужно отправить POST запрос по адресу https://ajax.test-danit.com/api/v2/cards. 
// Все, что вы напишете в теле запроса, попадет в базу данных как содержимое карточки.
    
    // Пример запроса (не обязательно отправлять именно такие данные):
    
    // fetch("https://ajax.test-danit.com/api/v2/cards", {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     'Authorization': `Bearer ${token}`
    //   },
    //   body: JSON.stringify({
    //     title: 'Визит к кардиологу',
    //     description: 'Плановый визит',
    //     doctor: 'Cardiologist',
    //     bp: '24',
    //     age: 23,
    //     weight: 70
    //   })
    // })
    //   .then(response => response.json())
    //   .then(response => console.log(response))
    
    // // Output
    // {
    //   "id": 1,
    //   "title": "Визит к кардиологу",
    //   "description": "Плановый визит",
    //   "doctor": "Cardiologist",
    //   "bp": "24",
    //   "age": 23,
    //   "weight": 70
    // }

// При успешном добавлении карточки в базу, в качестве ответа вы получите 
// переданный вами объект с добавлением "id" карточки в базе.

// Не забудьте сохранить в объекте, описывающем эту карточку, полученный "id".


// Важно!
// Обратите внимание, что любой запрос на сервер, 
// требующий отправки тела запроса (POST, PUT) обязательно должен сопровождаться 
// заголовком Content-Type со значением application/json.


// Получение всех созданных карточек

// Если вы хотите получить все созданные вами карточки,
//  необходимо отправить GET запрос по адресу https://ajax.test-danit.com/api/v2/cards, 
// указав ваш токен в заголовке запроса (подробнее см. выше). 
// В качестве ответа вы получите массив созданых вашей командой карточек в формате JSON, 
// после чего на их основе можно создать объекты 
// нужного класса (VisitDentist, VisitCardiologist, VisitTherapist и т.д.) и, 
// используя их метод render(), вывести их на экран.



// Получение одной карточки

// Если вы хотите получить данные об одной карточке, 
// необходимо отправить GET запрос по адресу https://ajax.test-danit.com/api/v2/cards/${cardId}, 
// указав в качестве ${cardId} уникальный номер карточки из базы данных.




// Изменение данных карточки

// Для обновления карточки после ее редактирования, 
// необходимо отправить PUT запрос по адресу https://ajax.test-danit.com/api/v2/cards/${cardId},
//  указав в качестве ${cardId} уникальный номер карточки из базы данных,
// а в теле запроса - новое содержимое карточки.

    // Пример запроса:
    // 
    // fetch("https://ajax.test-danit.com/api/v2/cards/1", {
    //   method: 'PUT',
    //   headers: {
        // 'Content-Type': 'application/json',
        // 'Authorization': `Bearer ${token}`
    //   },
    //   body: JSON.stringify({
        // id: 1,
        // title: 'Визит к кардиологу',
        // description: 'Новое описание визита',
        // doctor: 'Cardiologist',
        // bp: '24',
        // age: 23,
        // weight: 70
    //   })
    // })
    //   .then(response => response.json())
    //   .then(response => console.log(response))
    // 
    // Output
    // {
    //   "id": 1,
    //   "title": "Визит к кардиологу",
    //   "description": "Новое описание визита",
    //   "doctor": "Cardiologist",
    //   "bp": "24",
    //   "age": 23,
    //   "weight": 70
    // }

    // При успешном изменении содержимого карточки в базе данных,
    //  в качестве ответа вы получите переданный вами объект с измененным содержимым.